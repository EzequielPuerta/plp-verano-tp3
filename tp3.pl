% EJEMPLOS ----------------------------------------------------------------------------------------------

mapaEjemplo([
  bicisenda(arenales, retiro, 30),
  bicisenda(arenales, libertad, 20),
  bicisenda(retiro, libertad, 10),
  bicisenda(retiro, liniers, 10)]).

mapaEjemplo2([
  bicisenda(arenales, retiro, 30),
  bicisenda(arenales, libertad, 20),
  bicisenda(retiro, liniers, 10),
  bicisenda(libertad, constitucion, 25)]).

mapaNoConexo([
  bicisenda(arenales, retiro, 30),
  bicisenda(constitucion, santelmo, 15),
  bicisenda(arenales, libertad, 20),
  bicisenda(retiro, libertad, 10),
  bicisenda(retiro, liniers, 10)]).

mapaConCicloTrivial([
  bicisenda(arenales, retiro, 30),
  bicisenda(libertad, constitucion, 25),
  bicisenda(santelmo, constitucion, 15),
  bicisenda(arenales, libertad, 20),
  bicisenda(retiro, libertad, 10),
  bicisenda(constitucion, santelmo, 15),
  bicisenda(retiro, liniers, 10)]).

mapaConSendaTrivial([
  bicisenda(arenales, retiro, 30),
  bicisenda(arenales, libertad, 20),
  bicisenda(retiro, libertad, 10),
  bicisenda(libertad, constitucion, 25),
  bicisenda(retiro, liniers, 10),
  bicisenda(constitucion, constitucion, 15)]).

% EJERCICIO 1
% estaciones(+M, -Es)

% El predicado "estaciones" considera los casos donde la lista de bicisendas
% (es decir, el mapa) es vacía, ó cuando tiene elementos.
% En este caso se consideran 4 combinaciones.
% Dado el primer elemento, se agregan ambas estaciones a la solución parcial,
% ya que ninguna se encuentra en la solución recursiva del resto de la lista.
% En el segundo y tercer caso, solo se agrega una de las estaciones, ya que la
% restante ya se considera en la solución recursiva del resto de la lista.
% En el cuarto y último caso, se descartan ambas estaciones porque ya serán
% consideradas mas adelante en la solución recursiva del resto de la lista.

estaciones([], []).

estaciones([bicisenda(A,B,_)|ES], [A,B |RES]):-
  estaciones(ES,RES), not(member(A,RES)), not(member(B,RES)).

estaciones([bicisenda(A,B,_)|ES], [B |RES]):-
  estaciones(ES,RES), member(A,RES), not(member(B,RES)).

estaciones([bicisenda(A,B,_)|ES], [A |RES]):-
  estaciones(ES,RES), not(member(A,RES)), member(B,RES).

estaciones([bicisenda(A,B,_)|ES], RES):-
  estaciones(ES,RES), member(A,RES), member(B,RES).

%% EJERCICIO 2
% estacionesVecinas(+M,+E,-Es)

% El predicado "estacionesVecinas" indica qué estaciones se conectan directamente
% con la estación E según el mapa M. Dado un mapa vacío, las estaciones serán
% una lista vacía también. Por el contrario, si el mapa tiene bicisendas y el
% elemento actual tiene en alguno de sus extremos a la estación en cuestión, se
% agrega el otro extremo a la lista de resultados parciales. Si la bicisenda no
% tiene a la estación solicitada en ninguno de sus extremos, se la descarta y
% se continúa con la recusión.

estacionesVecinas([],_,[]).

estacionesVecinas([bicisenda(E,Y,_)|ES], E, [Y|T]):-
  estacionesVecinas(ES,E,T).

estacionesVecinas([bicisenda(X,E,_)|ES], E, [X|T]):-
  estacionesVecinas(ES,E,T).

estacionesVecinas([bicisenda(X,Y,_)|ES], E, T):-
  X\=E, Y\=E, estacionesVecinas(ES,E,T).

%% EJERCICIO 3
% distanciaVecinas(+M, +E1, +E2, -N)

% El predicado "distanciaVecinas" indica la distancia entre dos estaciones E1 y
% E2 que son directamente vecinas según el mapa M.
% Por lo tanto, se recorre la lista de bicisendas de M, y al encontrar la
% correcta (en cualquier sentido), se unifica el resultado con la distancia
% adecuada. Si la bicisenda no es la buscada, se la descarta y prosigue la recursión.

distanciaVecinas([bicisenda(A,B,DIST)|_], A, B, DIST).

distanciaVecinas([bicisenda(B,A,DIST)|_], A, B, DIST).

distanciaVecinas([bicisenda(X,Y,_)|ES], A, B, DIST):-
  X\=A, X\=B, distanciaVecinas(ES,A,B,DIST).

distanciaVecinas([bicisenda(X,Y,_)|ES], A, B, DIST):-
  Y\=A, Y\=B, distanciaVecinas(ES,A,B,DIST).

%% EJERCICIO 4
% caminoSimple(+M, +O, +D, ?C)
% caminoSimpleAux(+M, +O, +D, ?C, +V)

% El predicado "caminoSimple" unifica con C algún camino entre las estaciones
% O y D (que no contenga estaciones repetidas), que sea válido según las
% bicisendas de M. Requiere del predicado "caminoSimpleAux" que agrega a los
% parámetros una lista vacía, donde se irán acumulando las estaciones ya usadas.
% Recursivamente, caminoSimpleAux agrega la estación origen al camino solución.
% Luego agrega la misma estación a las usadas y comienza a probar la solución
% recursiva con alguna de las estaciones vecinas de la estación origen (que no
% haya sido agregada previamente a las usadas). Cuando se intenta consultar
% caminoSimpleAux de D a D, se devuelve el camino trivial [D].

caminoSimple(BS,O,D,C):-caminoSimpleAux(BS,O,D,C,[]).

caminoSimpleAux(_,D,D,[D],_).

caminoSimpleAux(BS,O,D,[O|C],XS):-
 append([O],XS,YS),
 estacionesVecinas(BS,O,ESV),
 member(E,ESV),
 not(member(E,YS)),
 caminoSimpleAux(BS,E,D,C,YS).

%% EJERCICIO 5
% mapaValido(+Bs)

% El predicado "mapaValido" indica si un mapa cumple con 3 condiciones.
% 1) Es conexo: para esto decimos que no pueden haber dos estaciones del mapa
%    que no puedan ser conectadas por medio de un camino simple.
% 2) No hay bicisendas de una estación a si misma: simplemente decimos que el
%    mapa no contiene bicisenda cuyos extremos correspondan a la misma estación.
% 3) No hay ciclos triviales: decimos que no pueden existir en el mapa, dos
%    bicisendas cuyos extremos sean los mismos pero en espejo.

mapaValido(BS):-
  not((estaciones(BS,ES), member(X,ES), member(Y,ES), X\=Y, not(caminoSimple(BS,X,Y,_)))), %% es conexo
  not(member(bicisenda(Z,Z,_),BS)), % no hay bicisendas de una estacion a si misma
  not((member(bicisenda(A,B,_),BS), member(bicisenda(B,A,_),BS))). %% no hay ciclos triviales

%% EJERCICIO 6
% caminoHamiltoniano(+M, +O, +D, ?C)

% El predicado "caminoHamiltoniano" unifica a C con un camino entre las estaciones
% O y D, posible según M, que no posee repetidos y que contiene a todas las
% estaciones del mapa. Para ello buscamos un camino simple entre O y D para M,
% donde no puede ocurrir que una estación de M no exista en el camino C.

caminoHamiltoniano(BS,O,D,C):-
  caminoSimple(BS,O,D,C), not((estaciones(BS,ES), member(E,ES), not(member(E,C)))).

%% EJERCICIO 7
% caminosHamiltonianos(+M, ?C)

% El predicado "caminosHamiltonianos" unifica a C con algún camino hamiltoniano
% del mapa M. Para ello aprovechamos caminoHamiltoniano y lo instanciamos con
% todos los pares posibles de estaciones de M.

caminosHamiltonianos(BS,C):-
  estaciones(BS,ES), member(X,ES), member(Y,ES), X\=Y, caminoHamiltoniano(BS,X,Y,C).

%% EJERCICIO 8
% caminoMinimo(+M, +O, +D, ?C, ?N)
% longitudCamino(+M, +C, ?N)

% El predicado "caminoMinimo" unifica a C y N con el camino mas corto entre O y
% D, según el mapa M. La distancia de este camino mas corto C, es N.
% Para ello, usamos el predicado "longitudCamino" que dado un mapa y un camino
% posible en él, indica su distancia. Lo hacemos reconstruyendo el camino según
% el mapa, tomando las bicisendas adecuadas para pasar de la estación iésima del
% camino, a la siguiente (sin importar el orden de los extremos). En el proceso,
% vamos sumando recursivamente las distancias de cada bicisenda usada.
% Finalmente, "caminoMinimo" es el camino simple C1, cuya distancia N1 no puede
% ser mayor para ningún otro camino C2 con distancia N2.

longitudCamino(_,[],0).

longitudCamino(_,[_],0).

longitudCamino(BS,[X,Y|C],DIST):-
  longitudCamino(BS,[Y|C],D),(member(bicisenda(X,Y,A),BS);member(bicisenda(Y,X,A),BS)),DIST is D + A.

caminoMinimo(BS,O,D,C1,N1):-
  caminoSimple(BS,O,D,C1), longitudCamino(BS,C1,N1),
  not((caminoSimple(BS,O,D,C2), longitudCamino(BS,C2,N2), N2<N1)).

% TESTS -------------------------------------------------------------------------------------------------

test(1) :- estaciones([], []).
test(2) :- mapaEjemplo(Mapa), estaciones(Mapa, [arenales, libertad, retiro, liniers]).
test(3) :- mapaNoConexo(Mapa), estaciones(Mapa, [constitucion, santelmo, arenales, libertad, retiro, liniers]).
test(4) :- mapaConCicloTrivial(Mapa), estaciones(Mapa, [arenales, libertad, constitucion, santelmo, retiro, liniers]).
test(5) :- mapaConSendaTrivial(Mapa), estaciones(Mapa, [arenales, libertad, retiro, liniers, constitucion, constitucion]).

test(6) :- mapaEjemplo(Mapa), estacionesVecinas(Mapa, retiro, [arenales,libertad,liniers]).
test(7) :- mapaEjemplo(Mapa), estacionesVecinas(Mapa, arenales, [retiro,libertad]).
test(8) :- mapaEjemplo(Mapa), estacionesVecinas(Mapa, libertad, [arenales,retiro]).
test(9) :- mapaEjemplo(Mapa), estacionesVecinas(Mapa, liniers, [retiro]).

test(10) :- mapaEjemplo(Mapa), distanciaVecinas(Mapa, retiro, arenales, 30), distanciaVecinas(Mapa, arenales, retiro, 30).
test(11) :- mapaEjemplo(Mapa), distanciaVecinas(Mapa, retiro, libertad, 10), distanciaVecinas(Mapa, libertad, retiro, 10).
test(12) :- mapaEjemplo(Mapa), not(distanciaVecinas(Mapa, libertad, liniers, N)).
test(13) :- mapaEjemplo(Mapa), not(distanciaVecinas(Mapa, liniers, libertad, N)).

test(14) :- mapaEjemplo2(Mapa), caminoSimple(Mapa, constitucion, liniers, [constitucion, libertad, arenales, retiro, liniers]).
test(15) :- mapaEjemplo2(Mapa), not(caminoSimple(Mapa, constitucion, liniers, [constitucion, arenales, retiro, liniers])).

test(16) :- mapaNoConexo(Mapa), not(mapaValido(Mapa)).
test(17) :- mapaConSendaTrivial(Mapa), not(mapaValido(Mapa)).
test(18) :- mapaConCicloTrivial(Mapa), not(mapaValido(Mapa)).
test(19) :- mapaEjemplo(Mapa), mapaValido(Mapa).
test(20) :- mapaEjemplo2(Mapa), mapaValido(Mapa).

test(21) :- mapaEjemplo2(Mapa), caminoHamiltoniano(Mapa, constitucion, liniers, [constitucion, libertad, arenales, retiro, liniers]).
test(22) :- mapaEjemplo(Mapa), caminoHamiltoniano(Mapa, liniers, libertad, [liniers, retiro, arenales, libertad]).
test(23) :- mapaNoConexo(Mapa), not(caminoHamiltoniano(Mapa, santelmo, retiro, C)).

test(24) :- mapaEjemplo(Mapa), caminosHamiltonianos(Mapa, [liniers, retiro, arenales, libertad]).
test(25) :- mapaEjemplo(Mapa), caminosHamiltonianos(Mapa, [libertad, arenales, retiro, liniers]).
test(26) :- mapaEjemplo(Mapa), caminosHamiltonianos(Mapa, [liniers, retiro, libertad, arenales]).
test(27) :- mapaEjemplo(Mapa), caminosHamiltonianos(Mapa, [arenales, libertad, retiro, liniers]).
test(28) :- mapaEjemplo(Mapa),
  not((
    not(caminosHamiltonianos(Mapa, [liniers, retiro, arenales, libertad])),
    not(caminosHamiltonianos(Mapa, [libertad, arenales, retiro, liniers])),
    not(caminosHamiltonianos(Mapa, [liniers, retiro, libertad, arenales])),
    not(caminosHamiltonianos(Mapa, [arenales, libertad, retiro, liniers])))).

test(29) :- mapaEjemplo(Mapa), caminoMinimo(Mapa, libertad, retiro, C, D), D is 10, C=[libertad, retiro].
test(30) :-
  mapaEjemplo(Mapa),
  C=[libertad, arenales, retiro],
  caminoSimple(Mapa, libertad, retiro, C),
  not(caminoMinimo(Mapa, libertad, retiro, C, D)).

tests :- forall(between(1,30,N), test(N)).
